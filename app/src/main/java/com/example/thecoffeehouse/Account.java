package com.example.thecoffeehouse;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;

public class Account extends AppCompatActivity {
    private TextView textView442;

    private ArrayList<Music> arrayList442;
    private CustomMusicAdapter adapter442;
    private ListView songList442;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);

        songList442 = (ListView) findViewById(R.id.listviewCauthu);
        arrayList442 = new ArrayList<>();
        arrayList442.add(new Music(R.drawable.ic_baseline_stars_24,"The Coffee House Rewards"));
        arrayList442.add(new Music(R.drawable.ic_person_outline_24,"Thông tin tài khoản "));
        arrayList442.add(new Music(R.drawable.ic_baseline_queue_music_24,"Nhạc đang phát"));
        arrayList442.add(new Music(R.drawable.ic_baseline_history_24,"Lịch sử"));
        arrayList442.add(new Music(R.drawable.ic_baseline_help_outline_24,"Giúp đỡ"));
        arrayList442.add(new Music(R.drawable.ic_baseline_settings_24,"Cài đặt"));

        adapter442 = new CustomMusicAdapter(this,R.layout.custom_music_item,arrayList442);
        songList442.setAdapter(adapter442);

        textView442 =(TextView) findViewById(R.id.tv_signup);
        textView442.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openLoginActivity();
            }
        });

        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView.setSelectedItemId(R.id.nav_person);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.nav_person:
                        return true;
                    case R.id.nav_news:
                        startActivity(new Intent(getApplicationContext(),HomeActivity.class));
                        overridePendingTransition(0,0);
                        return true;
                    case R.id.na_order:
                        startActivity(new Intent(getApplicationContext(),DatHang.class));
                        overridePendingTransition(0,0);
                        return true;
                }
                return false;
            }
        });
    }
    public void openLoginActivity(){
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);

    }
}
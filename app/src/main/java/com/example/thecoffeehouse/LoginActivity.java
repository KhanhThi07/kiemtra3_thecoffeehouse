package com.example.thecoffeehouse;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {
    private TextView textview442;
    private EditText etUsername442, etPassword442;
    private Button btLogin442;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        etUsername442= findViewById(R.id.et_use);
        etPassword442 = findViewById(R.id.et_pass);
        btLogin442 = findViewById(R.id.btnLogin);

        btLogin442.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validate(etUsername442.getText() .toString(),etPassword442.getText().toString());
            }
        });

        textview442 =(TextView) findViewById(R.id.txtSignup);
        textview442.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openSignupActivity1();
            }
        });
    }
    private void validate(String userName,String passWord ){
        if ((userName.equals("admin"))  && (passWord.equals("admin"))){
            openHomeActivity();
        }else{
            Toast.makeText(this.getApplicationContext(), "Sai mật khẩu hoặc tài khoản", Toast.LENGTH_SHORT).show();
        }
    }
    public void openSignupActivity1(){
        Intent intent = new Intent( this, SignupActivity.class);
        startActivity(intent);
    }
    public void openHomeActivity(){
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
    }
}

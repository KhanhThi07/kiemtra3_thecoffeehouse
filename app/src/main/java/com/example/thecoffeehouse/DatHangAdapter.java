package com.example.thecoffeehouse;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class DatHangAdapter extends BaseAdapter {

    private Context context442;
    private LayoutInflater inflater442;
    private String[] nameFood442, price442;
    private int[] foodImage442, addImage442;

    public DatHangAdapter(Context c442, String[] nameFood442, String[] price442, int[] foodImage442, int[] addImage442) {
        context442 = c442;
        this.nameFood442 = nameFood442;
        this.price442 = price442;
        this.foodImage442 = foodImage442;
        this.addImage442 = addImage442;
    }

    @Override
    public int getCount() {
        return nameFood442.length;
    }
    public int getCount1(){
        return price442.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater442 == null){
            inflater442 = (LayoutInflater) context442.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (convertView == null){
            convertView = inflater442.inflate(R.layout.grid_food, null);
        }

        ImageView imageView = convertView.findViewById(R.id.img_food);
        TextView textView = convertView.findViewById(R.id.tv_name);
        TextView textView1 = convertView.findViewById(R.id.tv_price);
        ImageView imageView1 = convertView.findViewById(R.id.add_food);

        imageView.setImageResource(foodImage442[position]);
        textView.setText(nameFood442[position]);
        textView1.setText(price442[position]);
        imageView1.setImageResource(addImage442[position]);

        return convertView;
    }
}

package com.example.thecoffeehouse;

public class UuDai {
    String name442, desc442, detail442, image442;

    public UuDai(String name442, String desc442,String detail442, String image442) {
        this.name442 = name442;
        this.desc442 = desc442;
        this.detail442 = detail442;
        this.image442 = image442;
    }

    public String getName442() {
        return name442;
    }

    public void setName442(String name442) {
        this.name442 = name442;
    }

    public String getDesc442() {
        return desc442;
    }

    public void setDesc442(String desc442) {
        this.desc442 = desc442;
    }

    public String getDetail442() {
        return detail442;
    }

    public void setDetail442(String detail442) {
        this.detail442 = detail442;
    }

    public String getImage442() {
        return image442;
    }

    public void setImage442(String image442) {
        this.image442 = image442;
    }
}

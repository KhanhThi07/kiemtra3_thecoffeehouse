package com.example.thecoffeehouse;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {
    Handler handler442;
    Runnable runnable442;
    ImageView img442;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        img442 = findViewById(R.id.img);
        img442.animate().alpha(4000).setDuration(0);

        handler442 = new Handler();
        handler442.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent dsp = new Intent(MainActivity.this,LoginActivity.class);
                startActivity(dsp);
                finish();
            }
        },4000);
    }
}
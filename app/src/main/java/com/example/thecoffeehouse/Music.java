package com.example.thecoffeehouse;


public class Music {
    private int image442;
    private String name442;

    public Music(int imageView, String name) {
        this.image442 = imageView;
        this.name442 = name;
    }

    public int getImage() {
        return image442;
    }

    public void setImage(int imageView) {
        this.image442 = imageView;
    }

    public String getName() {
        return name442;
    }

    public void setName(String name) {
        this.name442= name;
    }
}

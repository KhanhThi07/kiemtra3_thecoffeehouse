package com.example.thecoffeehouse;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class DatHang extends AppCompatActivity {
    GridView gridView442;

    String[] nameFood442 = {"Socola lúa mạch nóng","Socola lúa mạch nóng","Socola lúa mạch nóng","Socola lúa mạch nóng",
            "Socola lúa mạch nóng","Socola lúa mạch nóng","Socola lúa mạch nóng","Socola lúa mạch nóng"};
    String[] price = {"69 000 đ","69 000 đ","69 000 đ","69 000 đ",
            "69 000 đ","69 000 đ","69 000 đ","69 000 đ"};
    int[] foodImage442 = {R.drawable.logofood1,R.drawable.logofood1,R.drawable.logofood1,
            R.drawable.logofood1,R.drawable.logofood1,R.drawable.logofood1,R.drawable.logofood1,R.drawable.logofood1};
    int[] addImage442 = {R.drawable.ic_add_circle_outline_24,R.drawable.ic_add_circle_outline_24,R.drawable.ic_add_circle_outline_24,
            R.drawable.ic_add_circle_outline_24,R.drawable.ic_add_circle_outline_24,R.drawable.ic_add_circle_outline_24,
            R.drawable.ic_add_circle_outline_24,R.drawable.ic_add_circle_outline_24};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dat_hang);

        gridView442 = findViewById(R.id.grid_view);

        DatHangAdapter adapter = new DatHangAdapter(DatHang.this,nameFood442,price,foodImage442,addImage442);
        gridView442.setAdapter(adapter);

        gridView442.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getApplicationContext(),"You Clicked" + nameFood442[+position],
                        Toast.LENGTH_SHORT).show();
            }
        });

        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView.setSelectedItemId(R.id.na_order);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.na_order:
                        return true;
                    case R.id.nav_news:
                        startActivity(new Intent(getApplicationContext(),HomeActivity.class));
                        overridePendingTransition(0,0);
                        return true;
                    case R.id.nav_person:
                        startActivity(new Intent(getApplicationContext(),Account.class));
                        overridePendingTransition(0,0);
                        return true;
                }
                return false;
            }
        });


    }
}
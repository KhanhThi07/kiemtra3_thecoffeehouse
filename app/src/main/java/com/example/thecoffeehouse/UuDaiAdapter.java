package com.example.thecoffeehouse;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;

import java.util.List;

public class UuDaiAdapter extends RecyclerView.Adapter<UuDaiAdapter.UuDaiViewHolder>
{
    private List<UuDai> uuDaiList442;
    private Context context442;

    public UuDaiAdapter(Context context442, List<UuDai> uuDaiList442) {
        this.context442 = context442;
        this.uuDaiList442 = uuDaiList442;

    }

    @NonNull
    @Override
    public UuDaiViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        LayoutInflater inflater = LayoutInflater.from(context442);
        View view = inflater.inflate(R.layout.card_uudai,null);
        return new UuDaiViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull UuDaiViewHolder holder, int position)
    {
        UuDai uuDai442 = uuDaiList442.get(position);
        Glide.with(context442)
                .load(uuDai442.getImage442())
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(holder.img_km);

        holder.tv_name.setText(uuDai442.getName442());
        holder.tv_desc.setText(uuDai442.getDesc442());
        holder.tv_detail.setText(uuDai442.getDetail442());
    }

    @Override
    public int getItemCount() {
        return uuDaiList442.size();
    }

    public static class UuDaiViewHolder extends RecyclerView.ViewHolder
    {
        TextView tv_name, tv_desc, tv_detail;
        ImageView img_km;

        public UuDaiViewHolder(@NonNull View itemView) {
            super(itemView);

            tv_name = itemView.findViewById(R.id.tv_name);
            tv_desc = itemView.findViewById(R.id.tv_desc);
            tv_detail = itemView.findViewById(R.id.tv_detail);
            img_km = itemView.findViewById(R.id.img_km);

        }
    }
}

package com.example.thecoffeehouse;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.List;

public class HomeActivity extends AppCompatActivity {
    private RecyclerView recyclerView442;
    private List<UuDai> uuDaiList442 = new ArrayList<>();
    private LinearLayoutManager manager442;
    private UuDaiAdapter uuDaiAdapter442;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        recyclerView442 = findViewById(R.id.recycleView);
        setUuDaiAdapter();

        recyclerView442 = findViewById(R.id.recycleView2);
        setUuDaiAdapter();

        recyclerView442 = findViewById(R.id.recycleView3);
        setUuDaiAdapter();

        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView.setSelectedItemId(R.id.nav_news);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.nav_news:
                        return true;
                    case R.id.na_order:
                        startActivity(new Intent(getApplicationContext(),DatHang.class));
                        overridePendingTransition(0,0);
                        return true;
                    case R.id.nav_person:
                        startActivity(new Intent(getApplicationContext(),Account.class));
                        overridePendingTransition(0,0);
                        return true;
                }
                return false;
            }
        });

    }

    private void setUuDaiAdapter()
    {
        uuDaiList442.add(new UuDai("Giảm 50%, thèm gì gọi nhé Nhà mang tới liền","Hòa vào" +
                "không khí siêu sale cuối năm, mời team mình nghỉ tay gọi món yêu thích, Nhà giảm 50%" +
                "khi nhập mã MERRY50 - để lên tinh thần săn sale thoăn thoắt cùng:...","Chi tiết",
                "https://vuakhuyenmai.vn/wp-content/uploads/2020/09/thecoffeehouse-khuyen-mai-50off-23-9-2020.jpg"));
        uuDaiList442.add(new UuDai("Giảm 50%, thèm gì gọi nhé Nhà mang tới liền","Hòa vào" +
                "không khí siêu sale cuối năm, mời team mình nghỉ tay gọi món yêu thích, Nhà giảm 50%" +
                "khi nhập mã MERRY50 - để lên tinh thần săn sale thoăn thoắt cùng:...","Chi tiết",
                "https://vuakhuyenmai.vn/wp-content/uploads/2020/09/thecoffeehouse-khuyen-mai-50off-23-9-2020.jpg"));
        uuDaiList442.add(new UuDai("Giảm 50%, thèm gì gọi nhé Nhà mang tới liền","Hòa vào" +
                "không khí siêu sale cuối năm, mời team mình nghỉ tay gọi món yêu thích, Nhà giảm 50%" +
                "khi nhập mã MERRY50 - để lên tinh thần săn sale thoăn thoắt cùng:...","Chi tiết",
                "https://vuakhuyenmai.vn/wp-content/uploads/2020/09/thecoffeehouse-khuyen-mai-50off-23-9-2020.jpg"));
        uuDaiList442.add(new UuDai("Giảm 50%, thèm gì gọi nhé Nhà mang tới liền","Hòa vào" +
                "không khí siêu sale cuối năm, mời team mình nghỉ tay gọi món yêu thích, Nhà giảm 50%" +
                "khi nhập mã MERRY50 - để lên tinh thần săn sale thoăn thoắt cùng:...","Chi tiết",
                "https://vuakhuyenmai.vn/wp-content/uploads/2020/09/thecoffeehouse-khuyen-mai-50off-23-9-2020.jpg"));

        uuDaiAdapter442 = new UuDaiAdapter(HomeActivity.this,uuDaiList442);
        recyclerView442.setAdapter(uuDaiAdapter442);
        manager442 = new LinearLayoutManager(HomeActivity.this);
        manager442.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerView442.setLayoutManager(manager442);
    }
}
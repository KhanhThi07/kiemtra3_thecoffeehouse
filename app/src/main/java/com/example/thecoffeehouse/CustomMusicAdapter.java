package com.example.thecoffeehouse;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class CustomMusicAdapter extends BaseAdapter {
    private Context context442;
    private int layout442;
    private ArrayList<Music> arrayList442;
    public CustomMusicAdapter(Context context442, int layout442, ArrayList<Music> arrayList442){
        this.context442=context442;
        this.layout442 = layout442;
        this.arrayList442= arrayList442;
    }
    @Override
    public int getCount() {
        return arrayList442.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }
    private class ViewHolder {
        TextView textView;
        ImageView imageView;

    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        if(convertView == null){
            viewHolder = new ViewHolder();
            LayoutInflater layoutInflater = (LayoutInflater) context442.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = layoutInflater.inflate(layout442, null);
            viewHolder.textView = (TextView) convertView.findViewById(R.id.txtName);
            viewHolder.imageView = convertView.findViewById(R.id.ivMusic);
            convertView.setTag(viewHolder);
        }else {
            viewHolder =(ViewHolder) convertView.getTag();
        }
        Music music = arrayList442.get(position);

        viewHolder.textView.setText(music.getName());
        viewHolder.imageView.setImageResource(music.getImage());
        return convertView;
    }
}
